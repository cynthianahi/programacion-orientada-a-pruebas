const assert = require('chai').assert; // constante que permite utilizar chai
const main = require('../main'); // deja usar la clase primaria, fuera de la carpeta test
//npm install -g mocha
//npm i chai

const saludous = main.mensaje();
const comparacionnum = main.subtractionnum(4,2); //comparacion de dos numeros
const arrayofnumber = main.arrayofnumber();
const suma = main.sumandodosnum(5);


describe("main app" , function(){ // estructura donde almacena a las pruebas existentes

        
      
   //es una prueba que divide una oracion en 2            
        describe('contador',function(){
            it('cantidad de caracteres',function(){
            assert.equal("Mexico es Lindo".length,15);
        });
        });
        
        describe("Cadenas",function(){
        describe("subcadenas",function(){
           it('deberia retornar una sub cadenas',function(){
           assert.equal("Lindo","Mexico es Lindo".substring(10,15));
           })
        })
        });
    
        describe("Bienvenido usuario",function(){
           
               it('deberia retornar un saludo al usuario', function(){//se regresa terminal en la oracion como la descripcion de la prueba
                   assert.equal(saludous, "Bienvenido al codigo", "esto sera retornado");
               });   

               it('tipo de dato saludo ', function(){
                assert.typeOf(saludous, "String", "esto sera string?");
            });  
        });

        describe("substraccion resultado", function(){
           
            it('deberia retornar numeros', function(){//se regresa terminal en la oracion como la descripcion de la prueba
                assert.isBelow(comparacionnum, 5);
            });  
           
            it('tipo de dato numerico', function(){//se regresa terminal en la oracion como la descripcion de la prueba
                assert.typeOf(comparacionnum, "number", "este da un tipo numero?");
            });

        });

        describe("metodo de array, resultado", function(){
           
            it('deberia retornar arrays', function(){//se regresa terminal en la oracion como la descripcion de la prueba
                assert.include(arrayofnumber, 5);
            });  
           
            it('tipo de dato numerico', function(){//se regresa terminal en la oracion como la descripcion de la prueba
                assert.lengthOf(arrayofnumber, 6); //assertpermite usar los metodos
            });

        });



   

});
         